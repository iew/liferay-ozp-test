<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<script src="/ozp-iwc/js/ozpIwc-client.min.js"></script>

<script type="text/javascript">
var clientB = new ozpIwc.Client({peerUrl: "http://localhost:8080/ozp-iwc"});
var a=0;
    clientB.connect().then(function(){
        document.getElementById('outputB').innerHTML = "Client B connected with address: " + clientB.address;
    });
    var aRef = new clientB.data.Reference("/incA");
    var bRef = new clientB.data.Reference("/incB");

    function myBFunction() {
        console.log("click a++");
        a++;
        aRef.set( "Hello "+a);
    }
    bRef.watch(function(a){
        console.log("heard incB");
        document.getElementById("outputNumB").innerHTML = JSON.stringify(a);
    });
</script>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

This is the <b>liferay</b> B.
<p id="outputB"></p>
<button onclick="myBFunction()">A++</button>
<p id="outputNumB"></p>