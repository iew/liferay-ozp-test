<%--
/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<script src="/ozp-iwc/js/ozpIwc-client.min.js"></script>

<script type="text/javascript">
var clientA = new ozpIwc.Client({peerUrl: "http://localhost:8080/ozp-iwc"});
var b=0;
    clientA.connect().then(function(){
        document.getElementById('output').innerHTML = "Client A connected with address: " + clientA.address;
    });
    var aRef = new clientA.data.Reference("/incA");
    var bRef = new clientA.data.Reference("/incB");
    function myAFunction() {
        console.log("click b++");
        b++;
        bRef.set("Hello "+b);
    }
    aRef.watch(function(a){
        console.log("heard incA");
        document.getElementById("outputNumA").innerHTML = JSON.stringify(a);
    });
</script>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

This is the <b>liferay</b> A.
<p id="output"></p>
<button onclick="myAFunction()">B++</button>
<p id="outputNumA"></p>